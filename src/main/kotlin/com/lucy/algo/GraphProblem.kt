package com.lucy.algo

import kotlin.arrayOf as arr
import kotlin.intArrayOf as iarr

class GraphProblem

fun main(args:Array<String>){
    // Black -> 1
    // White -> 0
    val picture = arr(
        iarr(1,0,0,0,0,0),
        iarr(0,1,0,1,1,1),
        iarr(0,0,1,0,1,0),
        iarr(1,1,0,0,1,0),
        iarr(1,0,1,1,0,0),
        iarr(1,0,0,0,0,1),
    )
    print2D(picture)
    findBorderIsland(picture)
    println("===========>")
    print2D(picture)
}
fun print2D(data:Array<IntArray>) {
   println(data.map { it.joinToString(separator = ",") }.joinToString(separator = "\n"))
}
fun findBorderIsland(picture:Array<IntArray>): Array<IntArray> {
    val borders:MutableMap<Pair<Int,Int>, Boolean> = mutableMapOf()
    for(row in picture.indices) {
        for(column in 0 until  picture[row].size){
            if(isBorder(row,column,picture) && picture[row][column]==1){
                val key = borderPair(row, column)
                if(borders.containsKey(key))
                    continue
                borders[key]=true
                rec(row,column, borders, picture)
            }
        }
    }
    for(row in picture.indices) {
        for (column in 0 until picture[row].size) {
            val key = borderPair(row,column)
            if(picture[row][column]==1 && !borders.containsKey(key)){
                picture[row][column] = 0
            }
        }
    }

    return picture
}

fun rec(i:Int, j:Int, borders:MutableMap<Pair<Int,Int>, Boolean>, picture: Array<IntArray>) {
    val steps = arr(
        iarr(0,1),
        iarr(1,0),
        iarr(0,-1),
        iarr(-1,0)
    )
    for((first, second) in steps) {
        val rowX= first + i
        val rowY = second + j
        val key = borderPair(rowX, rowY)
        if(isBorderLimit(rowX,rowY, picture) || borders.containsKey(key))
            continue
        borders[key] = true
        val neighbors = picture[rowX][rowY]
        if(neighbors==1 ){
            rec(rowX,rowY, borders, picture)
        }
    }
}

fun borderPair(i:Int, j:Int): Pair<Int,Int> {
    return i to j
}

fun isBorder(i:Int, j:Int, picture: Array<IntArray>): Boolean {
     if(i == 0 || i== picture.size-1 || j==0 || j==picture[i].size-1)
         return true
    return false
}

fun isBorderLimit(i:Int, j:Int, picture: Array<IntArray>):Boolean {
    if(i < 0 || j < 0 || i > picture.size-1 || j > picture[i].size-1)
        return true
    return false
}