package com.lucy.algo.infrastructure.collection.service

import com.lucy.algo.entities.`interface`.collection.Elements
import com.lucy.algo.entities.domain.HeapType

class Heapify<T>(
    private val heapType: HeapType,
    private val conv: (HeapType, T, T) -> Boolean
): Elements<T> {
    private var elements: Array<T> = emptyArray<Any>() as Array<T>
    private var dc = 0
    private val simplified:(T, T) -> Boolean ={ src, dst -> conv(heapType, src, dst)}

    private val root:(Int) -> Int ={ x -> if((x / 2) > -1) x / 2 else 0 }

    private val leftChild:(Int) -> Int ={ x -> if((2*x + 1) <= dc) (2*x + 1) else dc}

    private val rightChild:(Int) -> Int ={ x -> if((leftChild(x)+1) <= dc) (leftChild(x) + 1) else dc}

    override fun insert(t: T) {
        TODO("Not yet implemented")
    }

    override fun insertAll(t: Array<T>) {
        elements = t
        dc = elements.size - 1
        heapify()
    }

    override fun delete() {
        if(dc >= 0) {
            swap(0, dc)
            dc = dc - 1
            reRootHeap(0)
        }
    }

    override fun show(): Array<T> {
        return elements
    }

    private fun heapify(){
       for(i in dc downTo  0){
           if(verifyNode(i)) {
               reRootHeap(i)
           }
       }
    }

    private fun verifyNode(pos: Int): Boolean {
        val left =  2 * pos + 1
        val right  = 2 * pos + 2
        return left <= dc || right <= dc
    }

    private fun reRootHeap(pos: Int) {
        if(pos < dc) {
            val leftPos = leftChild(pos)
            val rightPos = rightChild(pos)
            if (simplified(elements[leftPos], elements[rightPos])) {
                if (simplified(elements[leftPos], elements[pos])) {
                    swap(leftPos, pos)
                    reRootHeap(leftPos)
                }
            } else {
                if (simplified(elements[rightPos], elements[pos])) {
                    swap(rightPos, pos)
                    reRootHeap(rightPos)
                }
            }
        }
    }

    private fun swap(srcPos: Int, dstPos: Int) {
        if(srcPos <= dc && dstPos <= dc ) {
            val tmp = elements[srcPos]
            elements[srcPos] = elements[dstPos]
            elements[dstPos] = tmp
        }
    }
}