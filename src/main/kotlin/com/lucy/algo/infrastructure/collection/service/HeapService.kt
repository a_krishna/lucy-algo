package com.lucy.algo.infrastructure.collection.service

import com.lucy.algo.entities.`interface`.collection.Elements
import com.lucy.algo.entities.domain.HeapType

class HeapService<T>(
    private val heapType: HeapType,
    private val conv: (HeapType, T, T) -> Boolean
) : Elements<T> {
    private var elements: Array<T> = emptyArray<Any>() as Array<T>
    private var dc = -1

    private val simplifiedConv:(T, T) -> Boolean= {src, dst -> conv(heapType, src, dst) }

    private val root:(Int) -> Int ={ x -> if((x / 2) > -1) x / 2 else 0 }

    private val leftChild:(Int) -> Int ={ x -> if((2*x + 1) <= dc) (2*x + 1) else dc}

    private val rightChild:(Int) -> Int ={ x -> if((leftChild(x)+1) <= dc) (leftChild(x) + 1) else dc}

    override fun insert(t: T) {
        dc += 1
       elements = elements.plus(t)
       verifyHeap(dc)
    }

    override fun insertAll(t: Array<T>) {
       t.forEach { i -> insert(i) }
    }

    override fun delete() {
        if(dc > 0) {
            swap(dc, 0)
            dc-=1
            reRootHeap(0)
        }
    }

    override fun show(): Array<T> {
        return elements
    }

    private fun verifyHeap(pos:Int) {
        if(pos in 1..dc) {
            val rootPos = root(pos)
            if(simplifiedConv(elements[pos], elements[rootPos])){
                swap(pos, rootPos)
                verifyHeap(rootPos)
            }
        }
    }

    private fun reRootHeap(pos: Int) {
        if(pos < dc) {
            val leftPos = leftChild(pos)
            val rightPos = rightChild(pos)
            if (simplifiedConv(elements[leftPos], elements[rightPos])) {
                if (simplifiedConv(elements[leftPos], elements[pos])) {
                    swap(leftPos, pos)
                    reRootHeap(leftPos)
                }
            } else {
                if (simplifiedConv(elements[rightPos], elements[pos])) {
                    swap(rightPos, pos)
                    reRootHeap(rightPos)
                }
            }
        }
    }

    private fun swap(srcPos:Int, dstPos:Int) {
        if(srcPos <= dc && dstPos<=dc){
            val tmp = elements[dstPos]
            elements[dstPos] = elements[srcPos]
            elements[srcPos] = tmp
        }
    }

}