package com.lucy.algo.infrastructure.message.service

import com.fasterxml.jackson.databind.ObjectMapper
import com.lucy.algo.entities.`interface`.message.EmployeePublisher
import com.lucy.algo.entities.`interface`.message.PublishMessage
import com.lucy.algo.entities.domain.Employee
import com.lucy.algo.entities.domain.Message
import com.lucy.algo.infrastructure.message.config.KafkaProducers
import com.lucy.algo.infrastructure.message.model.KafkaMessage
import com.lucy.algo.infrastructure.message.model.KafkaMessageType
import com.lucy.algo.infrastructure.message.util.KafkaConstant
import com.lucy.algo.infrastructure.message.util.serialize
import org.apache.kafka.clients.producer.ProducerRecord
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kafka.sender.KafkaSender
import reactor.kafka.sender.SenderRecord
import java.util.logging.Logger

@Service
class SampleProducer constructor(
    private val sampleProducer1: KafkaSender<String, String>,
    private val producers: KafkaProducers,
    private val objectMapper: ObjectMapper
) : EmployeePublisher {
    private val logger = LoggerFactory.getLogger(SampleProducer::class.java)

    override fun sendMessage(t: Message): Mono<Unit> {
        return if(producers.producerConfig1[KafkaConstant.ENABLE] == true) {
            sampleProducer1.createOutbound()
                .send(Flux.just ( record(t) ))
                .then()
                .map { Unit }
                .doOnError { logger.error("Message Send Failed = ${it.message}") }
                .doOnNext { logger.info("Message Sent $it") }
                .doOnSuccess { logger.debug("Message SENT $it") }
        } else{
            Mono.empty()
        }
    }

    private fun record(message:Message): ProducerRecord<String, String> {
        val event = createKafkaMessage(message)
        return ProducerRecord(
            producers.producerConfig1[KafkaConstant.TOPIC].toString(),
            event.uid,
            event.serialize(objectMapper)
        )
    }

    private fun createKafkaMessage(message: Message): KafkaMessage {
        return KafkaMessage(type = KafkaMessageType.FLO, data = message.serialize(objectMapper))
    }
}