package com.lucy.algo.infrastructure.message.service

import com.fasterxml.jackson.databind.ObjectMapper
import com.lucy.algo.entities.`interface`.message.EmployeeConsumer
import com.lucy.algo.entities.domain.Employee
import com.lucy.algo.infrastructure.message.config.KafkaConsumers
import com.lucy.algo.infrastructure.message.model.KafkaMessage
import com.lucy.algo.infrastructure.message.model.KafkaMessageType
import com.lucy.algo.infrastructure.message.util.KafkaConstant
import com.lucy.algo.infrastructure.message.util.deserialize
import com.lucy.algo.infrastructure.message.util.toEmployee
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import reactor.kafka.receiver.KafkaReceiver
import javax.annotation.PostConstruct

@Service
class SampleConsumer constructor(
    private val sampleConsumer1: KafkaReceiver<String, String>,
    private val consumers: KafkaConsumers,
    private val employeeConsumer: EmployeeConsumer,
    private val objectMapper: ObjectMapper
) {
    private val logger = LoggerFactory.getLogger(SampleConsumer::class.java)

    @PostConstruct
    fun receiver() {
        if(consumers.consumerConfig1[KafkaConstant.ENABLE] == true) {
            sampleConsumer1
                .receive()
                .doOnNext {
                    logger.info("Received Event Key ${it.key()} value ${it.value()} offset: ${it.receiverOffset().offset()}")
                }
                .flatMap { recv ->
                    val event = recv.value().deserialize(objectMapper)
                    val defaultAck= Mono.just(recv.receiverOffset())
                    if(event!= null) {
                        when (event.type) {
                            KafkaMessageType.FLO ->
                            employeeConsumer.receive(event.data?.toEmployee(objectMapper)!!)
                                .map { recv.receiverOffset() }
                            KafkaMessageType.ALO -> defaultAck
                            KafkaMessageType.ACK -> defaultAck
                        }
                    } else{
                        defaultAck
                    }
                }
                .doOnNext { it.acknowledge() }
                .subscribe()
        }
    }
}