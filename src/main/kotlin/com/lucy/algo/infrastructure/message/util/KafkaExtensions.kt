package com.lucy.algo.infrastructure.message.util

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.JsonMappingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.lucy.algo.entities.domain.Employee
import com.lucy.algo.infrastructure.message.model.KafkaMessage

fun Any.serialize(objectMapper: ObjectMapper): String =
    objectMapper.writeValueAsString(this)

fun String.deserialize(objectMapper: ObjectMapper): KafkaMessage? {
    return try {
        objectMapper.readValue(this, KafkaMessage::class.java)
    }catch (e:Exception ){
        when(e){
            is JsonProcessingException, is JsonMappingException -> {null}
            else -> throw  e
        }
    }
}

fun String.toEmployee(objectMapper: ObjectMapper): Employee? {
    return try {
        objectMapper.readValue(this, Employee::class.java)
    }catch (e:Exception ){
        when(e){
            is JsonProcessingException, is JsonMappingException -> {null}
            else -> throw  e
        }
    }
}