package com.lucy.algo.infrastructure.message.model

import java.time.LocalDateTime
import java.time.ZoneOffset
import java.util.*

enum class KafkaMessageType {
    ALO, FLO, ACK
}

data class KafkaMessage (
    val uid:String = UUID.randomUUID().toString(),
    val type: KafkaMessageType,
    var data:String? = null,
    var timeStamp: Long =  LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)
)