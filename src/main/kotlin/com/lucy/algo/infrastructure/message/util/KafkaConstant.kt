package com.lucy.algo.infrastructure.message.util

object KafkaConstant {
    const val ENABLE = "enable"
    const val TOPIC = "topic"
    const val PARTITION = "partition"
    const val DEFAULT_PARTITION = 1

    const val DASH="-"
    const val DOT="."
}