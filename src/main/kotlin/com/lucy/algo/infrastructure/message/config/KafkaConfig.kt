package com.lucy.algo.infrastructure.message.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties(prefix = "kafka")
data class KafkaConfig(
   var producers: KafkaProducers,
   var consumers: KafkaConsumers
)

@Component
data class KafkaProducers  (
   var producerConfig1: Map<String, Any> = emptyMap()
)

@Component
data class KafkaConsumers(
    var consumerConfig1: Map<String, Any> = emptyMap()
)