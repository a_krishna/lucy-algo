package com.lucy.algo.infrastructure.message.config

import com.lucy.algo.infrastructure.message.util.KafkaConstant
import org.apache.kafka.common.TopicPartition
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import reactor.kafka.receiver.KafkaReceiver
import reactor.kafka.receiver.ReceiverOptions
import reactor.kafka.sender.KafkaSender
import reactor.kafka.sender.SenderOptions
import java.time.Duration
import java.util.*

@Configuration
class KafkaModule constructor(
    private val kafkaConfig: KafkaConfig
) {

    @Bean
    fun sampleProducer1(): KafkaSender<String, String> {
        return createSender(configFilter(kafkaConfig.producers.producerConfig1))
    }

    @Bean
    fun sampleConsumer1(): KafkaReceiver<String, String> {
        return  createReceiver(configFilter(kafkaConfig.consumers.consumerConfig1), getTopic(kafkaConfig.consumers.consumerConfig1))
    }

    private fun createReceiver(receiverConfig: Map<String, Any>, topic: String): KafkaReceiver<String, String> {
        return KafkaReceiver.create(
            ReceiverOptions.create<String?, String?>(receiverConfig)
                .subscription(Collections.singleton(topic))
        )
    }

    private fun createSender(senderConfig: Map<String, Any>): KafkaSender<String, String> {
        return KafkaSender.create(SenderOptions.create(senderConfig))
    }

    private fun configFilter(configs:Map<String, Any>): Map<String, Any> {
        return configs.filterKeys { !listOf(KafkaConstant.ENABLE, KafkaConstant.TOPIC).contains(it)}
            .map { it.key.replace(KafkaConstant.DASH, KafkaConstant.DOT) to  it.value }.toMap()
    }

    private fun getTopic(consumerConfig:Map<String, Any>): String {
        return consumerConfig[KafkaConstant.TOPIC].toString()
    }
}