package com.lucy.algo.infrastructure.search

import com.lucy.algo.entities.`interface`.search.FirstSearch
import com.lucy.algo.entities.`interface`.search.Graph
import com.lucy.algo.entities.domain.GraphType
import java.util.*

class GraphFirstSearch<U>(
    private val graphType: GraphType
) : Graph<U> , FirstSearch<U> {
    private val data:MutableMap<U, List<U>> = mutableMapOf()

    override fun bfs(u: U) {
        val visited:MutableMap<U, Boolean> = data.map { it.key to false }.toMap() as MutableMap<U, Boolean>
        val virtualQ: Queue<U> = LinkedList(listOf(u))
        visited[u]= true
        while (!virtualQ.isEmpty()){
            val u1 = virtualQ.poll()
            print(" $u1 ")
            data[u1]?.filter { visited[it] == false }
                ?.forEach {
                    virtualQ.add(it)
                    visited[it] = true
                }
        }

    }

    override fun dfs(u: U) {
        val visited:MutableMap<U, Boolean> = data.map { it.key to false }.toMap() as MutableMap<U, Boolean>
        val vStack: Stack<U> = Stack()
        vStack.push(u)
        visited[u]= true
        while (!vStack.isEmpty()) {
            val u1 = vStack.pop()
            print(" $u1 ")
            data[u1]?.filter { visited[it] == false }
                ?.forEach {
                    vStack.push(it)
                    visited[it] = true
                }
        }
    }

    override fun addEdge(u: U, v: U) {
        data[u] = data[u]?.plus(v) ?: listOf(v)
        if(graphType == GraphType.UNDIRECT) {
            data[v] = data[v]?.plus(u) ?: listOf(u)
        }
    }

    override fun isCyclic(): Boolean {
        val visited:MutableMap<U, Boolean> = data.map { it.key to false }.toMap() as MutableMap<U, Boolean>
        val recStack:MutableMap<U, Boolean> = data.map { it.key to false }.toMap() as MutableMap<U, Boolean>
        data.keys.filter { visited[it] == false }
            .forEach { u ->
                if(isCycleUtil(u, visited, recStack)){
                    return true
                }
            }
        return false
    }

    private fun isCycleUtil(u: U, visited:MutableMap<U, Boolean>, recStack:MutableMap<U, Boolean>): Boolean {
        visited[u] = true
        recStack[u] = true
        data[u]?.filter { visited[it] == false }
            ?.forEach {
            if(isCycleUtil(it, visited, recStack)){
                return  true
            } else return recStack[it] == true
        }
        recStack[u] = false
        return false
    }
}