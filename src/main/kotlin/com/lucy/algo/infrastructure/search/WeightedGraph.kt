package com.lucy.algo.infrastructure.search

import com.lucy.algo.entities.`interface`.search.Graph
import com.lucy.algo.entities.`interface`.search.ShortPathSearch
import com.lucy.algo.entities.`interface`.search.Vertex
import com.lucy.algo.entities.`interface`.search.Vertices
import com.lucy.algo.entities.domain.GraphType
import java.util.*

class WeightedGraph<U>(
    private val graphType: GraphType
) :  Vertex<U, Int>, Graph<U>, ShortPathSearch<U> {
    private val vertexes: MutableMap<U,Vertices<U,Int>> = mutableMapOf()
    private val graphVertexOrder:LinkedList<U> = LinkedList()

    override fun addEdge(u: U, v: U) {
        addWeightedEdges(u, v, 0)
    }

    override fun isCyclic(): Boolean {
        val visited = vertexes.map { it.key to false }.toMap() as MutableMap
        val recStack = vertexes.map { it.key to false }.toMap() as MutableMap
        vertexes.keys.filter { visited[it] == false }
            .forEach {
                if(isCycleUtil(it, visited, recStack)){
                    return true
                }
            }
        return false
    }

    override fun addVertex(u: U) {
        if(!vertexes.containsKey(u)){
            vertexes[u] = newVertex(u)
            graphVertexOrder.add(u)
        }
    }

    override fun addWeightedEdges(u: U, v: U, c: Int) {
       vertexes[u] = (vertexes[u] ?: newVertex(u)).addNeighbour(v, c)
//        graphVertexOrder.add(u)
        if(graphType == GraphType.UNDIRECT) {
            vertexes[v] = (vertexes[v] ?: newVertex(v)).addNeighbour(u, c)
        }
    }


    override fun dijkstra(vertex: U) {
        val distanceMap = vertexes.map { it.key to Int.MAX_VALUE }.toMap() as MutableMap
        val visited = vertexes.map { it.key to false }.toMap() as MutableMap
        distanceMap[vertex] = 0
        graphVertexOrder.forEach { cout ->
            val u = minPath(visited, distanceMap)
            if(u != null) {
                visited[u] = true
                graphVertexOrder.forEach { v ->
                    if( visited[u] == false && distanceMap[v]?.let {  it > distanceMap[u]!! + vertexes[u]!!.getWeight(v) } == true) {
                        distanceMap[v]  = distanceMap[u]!! + vertexes[u]!!.getWeight(v)
                    }
                }
            }
        }
        show(distanceMap, visited)
    }


     fun show(distanceMap: Map<U, Int>, visited: Map<U, Boolean>) {
        println("Vertex Distance from Source")
         println(vertexes.keys.map { "$it   ${distanceMap[it]}  ${visited[it]}" }.joinToString(separator = "\n"))
    }

    override fun allPath() {
        TODO("Not yet implemented")
    }

    private fun getDistance(distance: Long?): Long {
        return  distance ?: 0L
    }

    private fun minPath(visited: MutableMap<U, Boolean>, distance:MutableMap<U, Int>): U? {
        var minCost: Int = Int.MAX_VALUE
        var minPath:U? = null
        graphVertexOrder.forEach {
            if(distance[it]!! < minCost && visited[it] == false) {
                minCost = distance[it]!!
                minPath =  it
            }
        }
        return minPath
    }

    private fun newVertex(u: U): Vertices<U, Int> {
        return VerticesService(u, 0)
    }

    private fun isCycleUtil(u: U, visited: MutableMap<U, Boolean>, recStack: MutableMap<U, Boolean>): Boolean {
        visited[u] = true
        recStack[u] = true
        vertexes[u]?.getConnections()
            ?.filter { visited[it] == false }
            ?.forEach { child ->
                if(isCycleUtil(child, visited, recStack)){
                    return  true
                } else return recStack[child] == true
            }
        recStack[u] = false
        return  false
    }


}