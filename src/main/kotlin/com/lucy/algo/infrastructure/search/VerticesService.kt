package com.lucy.algo.infrastructure.search

import com.lucy.algo.entities.`interface`.search.Vertices

class VerticesService<U, C>(
    private val u: U,
    private val c: C
) : Vertices<U, C> {
    private val _id = u
    private val costMap: MutableMap<U, C> = mutableMapOf()

    override fun addNeighbour(u: U, c: C): Vertices<U, C> {
       costMap[u]= c
        return this
    }

    override fun getConnections(): List<U> {
       return costMap.keys.toList()
    }

    override fun getWeight(u: U): C {
       return costMap[u] ?: c
    }

    override fun getVertex(): U {
        return _id
    }
}