package com.lucy.algo.infrastructure.sort.service

import com.lucy.algo.entities.`interface`.sort.IntegerSort

class MergeSortService : IntegerSort {

    override fun sort(element: Array<Int>) {
        merge(element)
    }

    private fun merge(element: Array<Int>) {
        if(element.size > 1) {
            val mid = element.size / 2
            val left = element.copyOfRange(0, mid)
            val right = element.copyOfRange(mid, element.size)
            merge(left)
            merge(right)
            var i = 0
            var j = 0
            var k = 0
            while (i < left.size && j < right.size) {
                if(left[i] < right[j]){
                    element[k] = left[i]
                    i=i+1
                }else{
                    element[k] = right[j]
                    j=j+1
                }
                k= k+1
            }
            while (i < left.size) {
                element[k] = left[i]
                i=i+1
                k=k+1
            }
            while (j < right.size) {
                element[k] = right[j]
                j=j+1
                k=k+1
            }
        }
    }
}