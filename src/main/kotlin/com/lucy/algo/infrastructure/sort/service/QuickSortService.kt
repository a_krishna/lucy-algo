package com.lucy.algo.infrastructure.sort.service

import com.lucy.algo.entities.`interface`.sort.IntegerSort
import com.lucy.algo.entities.domain.SortType
import kotlin.random.Random

class QuickSortService(
    private val sortType: SortType
) : IntegerSort {

    override fun sort(element: Array<Int>) {
         quickSort(element, 0 , element.size-1)
    }

    private fun quickSort(element: Array<Int>, low: Int, high: Int) {
        if(low < high) {
            val pi = findPartition(element, low, high)
            quickSort(element, low, pi - 1)
            quickSort(element, pi + 1, high)
        }
    }
    private fun findPartition(array: Array<Int>, low:Int, high: Int): Int {
       return  when(sortType) {
            SortType.LOW -> lowPartition(array, low, high)
            SortType.HIGH -> highPartition(array, low, high)
            SortType.RANDOM -> randomPartition(array, low, high)
        }
    }

    private fun lowPartition(element: Array<Int>, low:Int, high: Int): Int {
        val pivot = element[low]
        var i = low + 1
        IntRange(low+1, high).forEach { j ->
            if(element[j] < pivot) {
                swap(element, i, j)
                i = i + 1
            }
        }
        swap(element, low, i-1)
        return i-1
    }

    private fun highPartition(element: Array<Int>, low:Int, high: Int): Int {
        val pivot = element[high]
        var i = low - 1
        IntRange(low, high).forEach { j ->
            if(element[j] < pivot){
                i= i+1
                swap(element, i, j)
            }
        }
        swap(element, i+1, high)
        return  i+1
    }

    private fun randomPartition(element: Array<Int>, low:Int, high: Int): Int {
       val r  = Random(high).nextInt(low, high)
        swap(element, r, high)
        return highPartition(element, low, high)
    }

    private fun swap(element: Array<Int>, src:Int, dest:Int) {
        val tmp = element[dest]
        element[dest] = element[src]
        element[src] = tmp
    }
}