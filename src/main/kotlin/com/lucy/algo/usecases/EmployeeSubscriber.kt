package com.lucy.algo.usecases

import com.fasterxml.jackson.databind.ObjectMapper
import com.lucy.algo.entities.`interface`.message.EmployeeConsumer
import com.lucy.algo.entities.domain.Employee
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono

@Component
class EmployeeSubscriber constructor(
    private val objectMapper: ObjectMapper
): EmployeeConsumer {
   private val logger = LoggerFactory.getLogger(EmployeeSubscriber::class.java)

    override fun receive(t: Employee): Mono<Boolean> {
        return Mono.fromCallable { t }
            .doOnNext { logger.info("Subscribed Msg $it") }
            .map { true }
    }
}