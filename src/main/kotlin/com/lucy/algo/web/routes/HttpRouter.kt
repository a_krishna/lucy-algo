package com.lucy.algo.web.routes

import com.lucy.algo.web.handler.MessageHandler
import org.springframework.core.io.ClassPathResource
import org.springframework.http.MediaType
import org.springframework.stereotype.Component

@Component
class HttpRouter constructor(
    private val messageHandler: MessageHandler
) {

    fun router() = org.springframework.web.reactive.function.server.router {
        "/lucy_algo/v1".nest {
            accept(MediaType.APPLICATION_JSON).nest {
                POST("/message", messageHandler::sendMessage)
            }

        }
        resources("/**", ClassPathResource("static/"))
    }
}