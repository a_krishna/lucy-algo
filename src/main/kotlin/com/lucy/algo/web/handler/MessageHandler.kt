package com.lucy.algo.web.handler

import com.lucy.algo.application.service.MessageService
import com.lucy.algo.entities.domain.Employee
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono

@Component
class MessageHandler constructor(
    private val messageService: MessageService
){

    fun sendMessage(serverRequest: ServerRequest): Mono<ServerResponse> {
        return serverRequest.bodyToMono(Employee::class.java)
            .flatMap {
                messageService.publishEmployee(it)
            }.flatMap { ServerResponse.ok().build() }
    }
}