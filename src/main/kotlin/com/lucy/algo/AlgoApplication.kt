package com.lucy.algo

import com.lucy.algo.entities.`interface`.sort.IntegerSort
import com.lucy.algo.entities.`interface`.sort.Sorting
import com.lucy.algo.entities.domain.HeapType
import com.lucy.algo.entities.domain.SortType
import com.lucy.algo.infrastructure.collection.service.HeapService
import com.lucy.algo.infrastructure.collection.service.Heapify
import com.lucy.algo.infrastructure.sort.service.MergeSortService
import com.lucy.algo.infrastructure.sort.service.QuickSortService
import java.util.*
import kotlin.collections.HashMap
import kotlin.arrayOf as arr
import kotlin.intArrayOf as iarr

class AlgoApplication
fun main(args: Array<String>) {
    val elements = arr(24,12,10,8,2,0,100)
    val elements2 = arr(10,5, 7, 9, 20, 8)
    val quickSort:Sorting<Int> = QuickSortService(SortType.RANDOM)
    println("----Quick Sort----")
    println(elements.joinToString(separator = ","))
    println("---------------")
    quickSort.sort(elements)
    println(elements.joinToString(separator = ",") )
    println("----"+elements2.joinToString(separator = ","))
    quickSort.sort(elements2)
    println("--s--"+elements2.joinToString(separator = ","))
    val elements1 = arr(24,12,10,8,2,0,100)
    val mergeSort:Sorting<Int> = MergeSortService()
    println("----Merge Sort----")
    println(elements1.joinToString(separator = ","))
    println("---------------")
    mergeSort.sort(elements1)
    println(elements1.joinToString(separator = ",") )
    println("========================<<HEAP>>=============================")
    val conv:(HeapType, Int, Int) -> Boolean = {  type, src, dst ->
        when (type){
            HeapType.MAX -> src > dst
            HeapType.MIN -> src < dst
        }
    }
    val maxHeap = HeapService<Int>(HeapType.MAX, conv)
    val minHeap = HeapService<Int>(HeapType.MIN, conv)
    val dataArray = arr(5, 10, 7, 12, 9, 0)
    maxHeap.insertAll(dataArray)
    minHeap.insertAll(dataArray)
    println("==== Max Heap data ===> ${maxHeap.show().joinToString(separator = ",")}")
    println("==== MinHeap data ===> ${minHeap.show().joinToString(separator = ",")}")
    println("===  Delete / Sort operation on heap ===")
    dataArray.forEach { maxHeap.delete() }
    println("==== Max Heap Sorted data ===> ${maxHeap.show().joinToString(separator = ",")}")
    dataArray.forEach { minHeap.delete() }
    println("==== Max Heap Sorted data ===> ${minHeap.show().joinToString(separator = ",")}")
    println("========================<<HEAPIFY>>=============================")
    val maxHeapify = Heapify<Int>(HeapType.MAX, conv)
//    val minHeapify = Heapify<Int>(HeapType.MIN, conv)
    val dataArray1 = arr(5, 10, 7, 12, 9, 0)
    maxHeapify.insertAll(dataArray1)
//    minHeapify.insertAll(dataArray1)
    println("==== Max Heapify data ===> ${maxHeapify.show().joinToString(separator = ",")}")
    dataArray1.forEach { maxHeapify.delete() }
    println("==== Max Heapify data ===> ${maxHeapify.show().joinToString(separator = ",")}")
//    println("==== Min Heapify data ===> ${minHeapify.show().joinToString(separator = ",")}")
   println("smallest  ${processSmallest(dataArray1.filter { it > 0 }.toTypedArray(), quickSort)}")
   println("smallest  ${processSmallest(arr(1,1,2,3).filter { it > 0 }.toTypedArray(), quickSort)}")
   println("smallest  ${processSmallest(arr(1,3,6,4,1,2).filter { it > 0 }.toTypedArray(), quickSort)}")
   println("smallest  ${processSmallest(arr(1,1,3,4,5,6).filter { it > 0 }.toTypedArray(), quickSort)}")
   println("smallest  ${processSmallest(arr(3, 1, 1,4,5,6).filter { it > 0 }.toTypedArray(), quickSort)}")
   println("Max Subset 1 ${findMaxSubSet(arr(1, -2, 0,9,-1,-2).toIntArray())}")
   println("Max Subset 2 ${findMaxSubSet(arr(3,1,8,2,5).toIntArray())}")

    println("Biggest Num [50552] = ${findBiggestNum("50552")}")
    println("Biggest Num [10101] = ${findBiggestNum("10101")}")
    println("Biggest Num [88] = ${findBiggestNum("88")}")

    println("Delete Counter [aaaabbbb] = ${findDeleteCounter("aaaabbbb")}")
    println("Delete Counter [example] = ${findDeleteCounter("example")}")
    println("MinCost [5,2,4,6,3,7] = ${finMinCost(arr(5,2,4,6,3,7).toIntArray(), 3, 6)}")
    println("LongestSubSequence [3,1,8,2,5] = ${findLongestSubsequence(arr(3,1,8,2,5).toIntArray())}")
    println("LongestSubSequence [1, -2, 0,9,-1,-2] = ${findLongestSubsequence(arr(1, -2, 0,9,-1,-2).toIntArray())}")
    println("MaxWays to Reach Home[4,3] = ${maxWays(4,3)}")
    println("MaxWays to Reach Home[2,2] = ${maxWays(2,2)}")
    println("MaxWays to Reach Home[1,1] = ${maxWays(1,1)}")
    println("MaxWays to Reach Home[100,50] = ${maxWays(100,50)}")
    println("MaxProfit Rod Cutter [0,2,4,5,7]{5} = ${maxProfitRodCutter(arr(0,2,4,5,7).toIntArray(),5)}")
    println("Share max unit profit [8,1,2,4,6,3]{5} = ${maxProfit(arr(8,1,2,4,6,3).toIntArray())}")
    println("Sum Range [1,-2,3,10,-8,0]{0,3} = ${sumRange(arr(1,-2,3,10,-8,0).toIntArray(), 0,3)}")
    println("Sum Range [1,-2,3,10,-8,0]{2,3} = ${sumRange(arr(1,-2,3,10,-8,0).toIntArray(), 2,3)}")
    println("Sum Range [1,-2,3,10,-8,0]{1,4} = ${sumRange(arr(1,-2,3,10,-8,0).toIntArray(), 1,4)}")
    println("Sum Range [1,-2,3,10,-8,0]{4,5} = ${sumRange(arr(1,-2,3,10,-8,0).toIntArray(), 4,5)}")
    println("Sum Range [1,-2,3,10,-8,0]{1,5} = ${sumRange(arr(1,-2,3,10,-8,0).toIntArray(), 1,5)}")
    println("Sum Range [1,-2,3,10,-8,0]{1,5} = ${sumRange(arr(1,-2,3,10,-8,0).toIntArray(), 1,5)}")
    println("Sum Range [1,-2,3,10,-8,0]{3,3} = ${sumRange(arr(1,-2,3,10,-8,0).toIntArray(), 3,3)}")
    println("Simple Fib = ${simpleFib(5)}")
    println("Claim Heaven [2,1,3,1,2]{5}  = ${minCostClaimHeaven(5, arr(2,1,3,1,2).toIntArray())}")
    println("MaxRob [3,8,10,4,1,7,2]    = ${maxRob(arr(3,8,10,4,1,7,2).toIntArray())}")
    println("cartesianMaxSplit [5]    = ${cartesianMaxSplit(5)}")
    val shapeArr = arr(
        iarr(4,5,3),
        iarr(2,3,2),
        iarr(3,6,2),
        iarr(1,5,4),
        iarr(2,4,1),
        iarr(1,2,2),
    )
    println("Find Max Stackable height = ${findMaxStack(shapeArr)}")
}


fun finMinCost(element:IntArray, k:Int, len:Int):Int {
//    element.sum()
    val maxElement = element.maxOrNull() ?: 0
    val storeCost = IntArray(len+1).map { Int.MAX_VALUE }.toTypedArray()
    storeCost[0] = 0
    IntRange(0, len-1).forEach { ith ->
        val freq = IntArray(maxElement +1).toTypedArray()
        IntRange(ith, len-1).forEach { jth ->
            freq[element[jth]]+=1
            var cost =0
            IntRange(0, maxElement).forEach { kth ->
                    cost+= if(freq[kth] == 1) 0 else freq[kth]
            }
            storeCost[jth + 1] = Math.min(storeCost[ith] +   cost + k, storeCost[jth + 1])
        }
    }
    return storeCost[len]
}
fun findDeleteCounter(data:String):Int {
    val lMap:MutableMap<Char,Int> = HashMap<Char, Int>()
    val frequency: PriorityQueue<Int> = PriorityQueue<Int>( compareByDescending { it })
    var charCount = 0
    data.forEach {
        lMap[it] = lMap[it]?.plus(1) ?: 0
    }
    lMap.values.forEach {frequency.add(it)}
    while (frequency.size > 0){
        val freq = frequency.peek()
        frequency.remove()
        if(frequency.isEmpty()){
            return charCount
        }
        if(freq ==  frequency.peek()){
            if(freq > 1){
                frequency.add(freq -1)
            }
            charCount+=1
        }
    }
    return charCount
}
fun findBiggestNum(data: String): Int {
    var max_int = Int.MIN_VALUE
    IntRange(0, data.length - 2) .forEach{ index ->
        val num =  data.substring(index, index+2)
        max_int = Math.max(max_int, num.toInt())
    }
    return max_int
}

fun processSmallest(data:Array<Int>, sort:Sorting<Int>):Int {
    sort.sort(data)
    return findSmallest(data)
}
fun findSmallest(data: Array<Int>): Int {
    val list = LinkedList<Int>()
    var max_int = 1
     data.mapIndexed{ index, e ->
        max_int=  Math.max(max_int, e)
       val e1 = if(index == 0){
           e - 0
       } else {
         Math.abs(e - data[index -1])
       }
       if(e1 > 1) {
         list.add(e-1)
       }
    }
    return if (list.isEmpty()) max_int + 1 else list.last
}

fun  findMaxSubSet(n:IntArray): Int {
    val diesNS=6
    val minValue = Int.MIN_VALUE
    val subArray = IntArray(n.size + diesNS) {  minValue }
    subArray[diesNS] = n[0]
    IntRange(diesNS+1, (n.size+diesNS)-1).forEach { idx ->
        var maxValue = minValue
        IntRange(0,diesNS-1).forEach { subidx->
            maxValue = Math.max(maxValue, subArray[idx-subidx-1])
        }
        subArray[idx] = n[idx-diesNS] + maxValue
    }
   return subArray[n.size+diesNS -1]
}

fun findGap(data: String): Int {
    var d1=0
    var fc= 0
    data.mapIndexed{ index, c ->
        if(c == '1') {
            if(index > 0 && data[index-1] !='1')
                d1= Math.max(d1, fc)
            fc=0
        } else {
            fc+=1
        }
    }
    return d1
}

//3,1,8,2,5
//0,1,2,3,4
//1,1,2,2,3
fun findLongestSubsequence(n: IntArray): Int{
    val minValue = IntArray(n.size) { 1 }
    IntRange(1, minValue.size-1).forEach{ i ->
        val nth = IntRange(0, i).filter{j -> n[j] < n[i]}.map{minValue[it]}
        minValue[i] = 1 + (nth.maxOrNull() ?: 0)
    }
    return minValue.maxOrNull() ?: 0
}

//Reach Home Max Ways
fun maxWays(m:Int, n:Int): Int {
    val ways = Array(m) {IntArray(n)}
    IntRange(0, m-1).forEach { ways[it][n-1] = 1 }
    IntRange(0, n-1).forEach { ways[m-1][it] = 1 }
    for(i in m-2 downTo 0){
        for (j in n-2 downTo 0){
            ways[i][j]= ways[i][j+1]+ ways[i+1][j]
        }
    }
    return ways[0][0]
}

fun maxProfitRodCutter(cost:IntArray, n:Int): Int {
    val rods = IntArray(n)
    rods[0] = 0
    IntRange(1, n-1).forEach { i->
        var max_val = Int.MIN_VALUE
        IntRange(1, i).forEach{ j ->
            max_val =Math.max(max_val,cost[j] + rods[i-j])
        }
        rods[i] = max_val
    }

    return rods[n-1]
}

fun maxProfit(prices:IntArray): Int {
    val min_util = IntArray(prices.size){ Int.MIN_VALUE}
    var highestProfit = Int.MIN_VALUE
    min_util[0] = prices[0]
    IntRange(1, prices.size-1).forEach { min_util[it] = Math.min(min_util[it-1], prices[it]) }
    IntRange(1 , prices.size -1).forEach { i->
        highestProfit = Math.max(highestProfit,prices[i] - min_util[i])
    }
    return highestProfit
}

fun sumRange(gArray:IntArray, m:Int, n:Int): Int {
   return if(m <= n && n < gArray.size){
       val sum_until = IntArray(n+1)
       sum_until[0] = gArray[0]
       IntRange(1,n).forEach { sum_until[it] = sum_until[it-1] + gArray[it] }
       if(m == 0) sum_until[n] else sum_until[n] - sum_until[m-1]
   } else{
     0
   }
}

fun simpleFib(n:Int): Int {
    var a = 1
    var b = 1
    if(n > 1)
        IntRange(2, n).forEach {
            b = a + b
            a = b - a
        }
    return b
}

fun minCostClaimHeaven(n:Int, cost:IntArray): Int {
    val min_fee = IntArray(n)
    min_fee[0] = 0
    min_fee[1] = cost[0]
    min_fee[2] = cost[0]
    IntRange(3, n-1).forEach {
        min_fee[it] =  Math.min(min_fee[it-1]+ cost[it-1],Math.min(min_fee[it-2]+ cost[it-2], min_fee[it-3]+ cost[it-3]))
    }
    return min_fee[n-1]
}

fun maxRob(houses:IntArray):Int {
    val max_money = IntArray(houses.size+1)
    max_money[houses.size] = 0
    max_money[houses.size-1] = houses[houses.size-1]
    for(i in houses.size-2 downTo 0){
        max_money[i] = Math.max(houses[i]+ max_money[i+2], max_money[i+1])
    }
    return max_money[0]
}

fun cartesianMaxSplit(n:Int): Int {
    val max_split = IntArray(n+1){Int.MIN_VALUE}
    max_split[1]  = 0
    max_split[2] = 1
    for( i in 3..n){
        for(j in 1..i-1){
            max_split[i] = Math.max(j* max_split[i-j], Math.max(j* (i-j), max_split[i]))
        }
    }
    return max_split[n]
}

fun findMaxStack(boxes:Array<IntArray>):Int {
    boxes.sortBy { it-> it[0] }
    val heights = boxes.map { it to it[2] }.toMap() as MutableMap<IntArray, Int>
    IntRange(1, boxes.size-1).forEach { i ->
        val box = boxes[i]
        val S = IntRange(0, i).filter { isStackable(boxes[it], box) }.map { j -> boxes[j] }
        heights[box] = box[2] + (S.map { heights[it] ?: 0 }.maxOrNull() ?: 0)
    }
    return heights.values.maxOrNull() ?: 0
}

fun isStackable(top:IntArray, bottom:IntArray): Boolean {
   return top[0] < bottom[0] && top[1] < bottom[1]
}