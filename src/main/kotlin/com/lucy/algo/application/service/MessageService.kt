package com.lucy.algo.application.service

import com.lucy.algo.entities.`interface`.message.EmployeePublisher
import com.lucy.algo.entities.domain.Employee
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class MessageService constructor(
    private val employeePublisher: EmployeePublisher
) {

    fun publishEmployee(employee: Employee): Mono<Unit> {
        return employeePublisher.sendMessage(employee)
    }
}