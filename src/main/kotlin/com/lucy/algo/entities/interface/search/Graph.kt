package com.lucy.algo.entities.`interface`.search

interface Graph<U> {

    fun addEdge(u:U, v:U)

    fun isCyclic(): Boolean
}