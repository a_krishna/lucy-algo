package com.lucy.algo.entities.`interface`.message

import com.lucy.algo.entities.domain.Message
import reactor.core.publisher.Mono

interface PublishMessage<T: Message> {

    fun sendMessage(t:T): Mono<Unit>
}