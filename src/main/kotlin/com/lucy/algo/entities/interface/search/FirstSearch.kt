package com.lucy.algo.entities.`interface`.search

interface FirstSearch<U> {

    //Breadth First Search
    fun bfs(u: U)

    //Depth First Search
    fun dfs(u: U)
}