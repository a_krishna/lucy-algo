package com.lucy.algo.entities.`interface`.sort

sealed interface Sorting<T> {
    fun sort(element:Array<T>): Unit
}