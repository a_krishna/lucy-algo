package com.lucy.algo.entities.`interface`.collection

interface Elements<T> {

    fun insert(t: T)

    fun insertAll(t: Array<T>)

    fun delete()

    fun show():Array<T>
}