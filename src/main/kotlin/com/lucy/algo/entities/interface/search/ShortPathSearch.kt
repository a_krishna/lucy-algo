package com.lucy.algo.entities.`interface`.search

interface ShortPathSearch<U> {

    //  Single Source of Path from vertex O(n2)
    fun dijkstra(vertex: U)

    // Single Source of Path from all the vertex O(n3) dynamic programming
    fun allPath()
}