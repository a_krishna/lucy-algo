package com.lucy.algo.entities.`interface`.message

import com.lucy.algo.entities.domain.Message
import reactor.core.publisher.Mono

interface ConsumeMessage<T:Message> {

    fun receive(t:T): Mono<Boolean>
}