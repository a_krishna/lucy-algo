package com.lucy.algo.entities.`interface`.message

import com.lucy.algo.entities.domain.Employee
import com.lucy.algo.entities.domain.Message

interface EmployeePublisher : PublishMessage<Message>