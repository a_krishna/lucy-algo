package com.lucy.algo.entities.`interface`.search

interface Vertices<U, C> {

    fun addNeighbour(u: U, c: C): Vertices<U, C>

    fun getConnections():List<U>

    fun getWeight(u: U): C

    fun getVertex(): U
}