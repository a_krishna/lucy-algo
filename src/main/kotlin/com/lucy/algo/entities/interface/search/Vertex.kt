package com.lucy.algo.entities.`interface`.search

interface Vertex<U, C> {

    fun addVertex(u: U)

    fun addWeightedEdges(u:U, v: U, c: C)

}