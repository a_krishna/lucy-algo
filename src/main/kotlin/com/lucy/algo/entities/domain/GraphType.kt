package com.lucy.algo.entities.domain

enum class GraphType {
    DIRECT, UNDIRECT
}