package com.lucy.algo.entities.domain

enum class SortType {
    LOW , HIGH, RANDOM
}