package com.lucy.algo.entities.domain

sealed interface Message


enum class MessageType {
    ALO, FLO, ACK
}

data class Employee(
    val empId: String?= null,
    val name: String?= null,
    val department: String?= null
): Message