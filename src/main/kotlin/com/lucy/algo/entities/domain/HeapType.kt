package com.lucy.algo.entities.domain

enum class HeapType {
    MIN, MAX
}