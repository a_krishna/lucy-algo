package com.lucy.algo

import com.lucy.algo.entities.domain.GraphType
import com.lucy.algo.infrastructure.search.GraphFirstSearch
import com.lucy.algo.infrastructure.search.WeightedGraph

class SearchTest

fun main(args: Array<String>) {
    val graph = GraphFirstSearch<Int>(GraphType.UNDIRECT)
    val weightedGraph = WeightedGraph<Int>(GraphType.DIRECT)

//    graph.addEdge(5,7)
//    graph.addEdge(5,10)
//    graph.addEdge(10,12)
//    graph.addEdge(10,9)
//    graph.addEdge(7,0)
//
//    println("=============GRAPH =================")
//    println("====== BFS=========")
    graph.bfs(5)
//    println("===========DFS ========")
//    graph.dfs(5)
//    println()
//    println("Cycle ?==? ${graph.isCyclic()}")

    println("============= Weighted Graph===============")
    weightedGraph.addVertex(0)
    weightedGraph.addVertex(1)
    weightedGraph.addVertex(2)
    weightedGraph.addVertex(3)
    weightedGraph.addVertex(4)
    weightedGraph.addVertex(5)
     weightedGraph.addWeightedEdges(0, 2, 3)
     weightedGraph.addWeightedEdges(0, 3, 2)
     weightedGraph.addWeightedEdges(1, 4, 7)
     weightedGraph.addWeightedEdges(2, 3, 5)
     weightedGraph.addWeightedEdges(2, 4, 2)
     weightedGraph.addWeightedEdges(3, 1, 1)
     weightedGraph.addWeightedEdges(4, 5, 1)
    println("============Shorts Path====================")
    weightedGraph.dijkstra(0)

}