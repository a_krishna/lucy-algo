import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.3.3.RELEASE"
    id("io.spring.dependency-management") version "1.0.10.RELEASE"
    kotlin("jvm") version "1.5.30"
    kotlin("plugin.spring") version "1.5.30"
    id("org.jlleitschuh.gradle.ktlint") version "9.3.0"
    application
}

group = "com.lucy.algo"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}

application {
    mainClass.set("com.lucy.algo.ApplicationKt")
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-data-couchbase-reactive")
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("org.springframework.integration:spring-integration-webflux")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
    implementation("io.projectreactor.kafka:reactor-kafka:1.2.6.RELEASE")
//    implementation("io.springfox:springfox-swagger2:3.0.0-SNAPSHOT")
//    implementation("io.springfox:springfox-swagger-ui:3.0.0-SNAPSHOT")
//    implementation("io.springfox:springfox-spring-webflux:3.0.0-SNAPSHOT")
//    implementation("io.springfox:springfox-spring-integration-webflux:3.0.0-SNAPSHOT")
    implementation("org.apache.poi:poi:4.1.1")
    implementation("org.apache.poi:poi-ooxml:4.1.1")
    implementation("com.monitorjbl:xlsx-streamer:2.1.0")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
    implementation("com.couchbase.client:java-client:3.0.7")
    developmentOnly("org.springframework.boot:spring-boot-devtools")
    annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
    }
    testImplementation("io.projectreactor:reactor-test")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=stridevtoolsct")
        jvmTarget = "11"
    }
}
